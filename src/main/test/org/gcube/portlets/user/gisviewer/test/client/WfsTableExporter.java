package org.gcube.portlets.user.gisviewer.test.client;

import java.util.Map;

import org.gcube.portlets.user.gisviewer.client.commons.beans.LayerItem;
import org.gcube.portlets.user.gisviewer.server.datafeature.wfs.WFSExporter;
import org.gcube.portlets.user.gisviewer.server.datafeature.wfs.WFSExporter.WFS_TO;
import org.gcube.portlets.user.gisviewer.shared.CSVFile;
import org.gcube.portlets.user.gisviewer.shared.FeatureExportViewConfig;

public class WfsTableExporter {
	
	
	public static void printMap(Map<String, FeatureExportViewConfig> theMap) {
		for (String key : theMap.keySet()) {
			System.out.println("Key: "+key + " - value: "+theMap.get(key));
			
		}
	}
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		WFSExporter exporter = new WFSExporter();
		System.out.println("\n\nEXPORTER TO WFS VIEW: ");
		printMap(exporter.getWFSExporterConfigTo(WFS_TO.VIEW).getMap());
		
		System.out.println("\n\nEXPORTER TO WFS EXPORT: ");
		printMap(exporter.getWFSExporterConfigTo(WFS_TO.EXPORT).getMap());
		
		String bbox = "-90,-180,90,180";
		int maxFeatures = 200;
		
		LayerItem l = new LayerItem();

		l.setLayer("sdilab_wheintz:wheintz1");
		l.setGeoserverUrl("http://geoserver-sdi-lab.d4science.org/geoserver/sdilab_wheintz");
		l.setName("sdilab_wheintz:wheintz1");
		

		l.setLayer("geona-proto:schede_sub");
		l.setGeoserverUrl("https://geona-proto.d4science.org/geoserver/geona-proto/wms");
		l.setName("geona-proto:schede_sub");
		
		l.setLayer("geona-proto:schede_sub");
		l.setGeoserverUrl("https://geona-proto.d4science.org/geoserver/geona-proto/wms");
		l.setName("geona-proto:schede_sub");
		bbox = "40.9130859375,13.194580078125,41.46240234375,13.919677734375";
		l.setCqlFilter("ANNO_DA<1942");

		CSVFile table = exporter.getTableFromJsonToExport(l, bbox, maxFeatures, WFS_TO.VIEW);
//
		System.out.println("Result: "+table.toString());
		
		
		/*List<LayerItem> layerItems = new ArrayList<LayerItem>();
		LayerItem l;

		l = new LayerItem();
		l.setLayer("aquamaps:depthMean");
		l.setGeoserverUrl("http://geoserver.d4science-ii.research-infrastructures.eu/geoserver");
		l.setName("Depth Mean");
		layerItems.add(l);

		l = new LayerItem();
		l.setLayer("aquamaps:environments");
		l.setGeoserverUrl("http://geoserver2.d4science.research-infrastructures.eu/geoserver");
		l.setName("Environments");
		layerItems.add(l);

		l = new LayerItem();
		l.setLayer("aquamaps:eezall");
		l.setGeoserverUrl("http://geoserver.d4science-ii.research-infrastructures.eu/geoserver");
		l.setName("Eezall");
		layerItems.add(l);

		List<WfsTable> list = FeatureParser.getDataResults(layerItems, "86,0,180,1", 5);

		for (WfsTable table: list) {
			System.out.println("TABLE \""+ table.getTitle() +"\"");

			List<String> columnNames = table.getColumnNames();
			for (String columnName : columnNames)
				System.out.print(columnName+", ");
			System.out.println();

			for (BaseModel row : table.getRows()) {
				for (String columnName : columnNames)
					System.out.print(row.get(columnName)+", ");
				System.out.println();
			}
		}*/

//		LayerItem l = new LayerItem();
//		l.setLayer("spd538430dc08474934a4b62ffb03edd483");
//		l.setGeoserverUrl("http://geoserver-dev.d4science-ii.research-infrastructures.eu/geoserver/wfs");
//		l.setName("spd538430dc08474934a4b62ffb03edd483");
//
//		WebFeatureTable table = FeatureParser.getTableFromJson(l, "-90,-180,90,180", 1);
//
//		System.out.println("Result: "+table.toString());
		
	}

}
