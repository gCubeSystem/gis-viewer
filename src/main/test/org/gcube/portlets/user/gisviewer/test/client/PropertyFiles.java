package org.gcube.portlets.user.gisviewer.test.client;

import java.util.List;

import org.gcube.portlets.user.gisviewer.server.baselayer.BaseLayerPropertyReader;
import org.gcube.portlets.user.gisviewer.server.exception.PropertyFileNotFoundException;
import org.gcube.portlets.user.gisviewer.shared.GisViewerBaseLayer;

public class PropertyFiles {

	public static void main(String[] args) throws PropertyFileNotFoundException {
		List<? extends GisViewerBaseLayer> layers = BaseLayerPropertyReader.readBaseLayers();

		for (GisViewerBaseLayer gisViewerBaseLayer : layers) {
			System.err.println(gisViewerBaseLayer);
		}
	}

}
