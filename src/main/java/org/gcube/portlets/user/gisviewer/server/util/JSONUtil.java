package org.gcube.portlets.user.gisviewer.server.util;

import org.json.JSONObject;

/**
 * The Class JSONUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Apr 30, 2020
 */
public class JSONUtil {
	
	public static String DOUBLE_QUOTES = "\"";
	
    /**
     * Try to convert a string into a number (double, long, integer) or boolean.
     * If the string can't be converted, return the string between double quotes
     * otherwise return the input string
     * 
     * eg. Naples (is a string) -> "Naples"
     *     20 (is an number) -> 20
     *
     */
	
	public static String stringToValueQuoted(String valueString) {

		if (valueString == null || valueString.isEmpty())
			return "";

		Object theValueObj = JSONObject.stringToValue(valueString);
		String theValue = null;
		if (theValueObj instanceof Boolean 
				|| theValueObj instanceof Long 
				|| theValueObj instanceof Double 
				|| theValueObj instanceof Integer) {
			theValue = theValueObj.toString(); // I'm not adding the double quotes;
		} else {
			theValue = String.format("%s%s%s", DOUBLE_QUOTES, theValueObj, DOUBLE_QUOTES);
		}

		return theValue;

	}
	
}
