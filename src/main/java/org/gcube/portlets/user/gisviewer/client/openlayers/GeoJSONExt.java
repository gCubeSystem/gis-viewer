package org.gcube.portlets.user.gisviewer.client.openlayers;

import org.gwtopenmaps.openlayers.client.format.FormatImpl;
import org.gwtopenmaps.openlayers.client.format.GeoJSON;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.core.client.GWT;


// TODO: Auto-generated Javadoc
/**
 * The Class GeoJSONExt.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 12, 2020
 */
public class GeoJSONExt extends GeoJSON{
	
    /**
     * Instantiates a new geo JSON ext.
     */
    public GeoJSONExt()
    {
        super();
    }
    

    /**
     * Parses the geometry.
     *
     * @param geoJson the geo json
     * @return the geometry
     */
    public Geometry toGeometry(String geoJson){
    	
    	String wktString = geoJSONtoWKT(geoJson);
    	Geometry theGeom = Geometry.fromWKT(wktString);
    	GWT.log("the theGeom read as WKT is: "+theGeom);
    	return theGeom;
  
    }
    

    /**
     * Gets the centroid from geo JSON.
     *
     * @param geoJson the geo json
     * @return the centroid from geo JSON
     */
    public Point getCentroidFromGeoJSON(String geoJson){
    	
    	JSObject jsonObj = getCentroidForGeoJSON(geoJson);
    	return narrowToPoint(jsonObj);
  
    }
    
    /**
     * Parses the geometry.
     *
     * @param geoJson the geo json
     * @return the geometry
     */
    public Geometry toGeometryCentroid(String geoJson){
    	
    	String wktString = geoJSONtoWKT(geoJson);
    	Geometry theGeom = Geometry.fromWKT(wktString);
    	GWT.log("the theGeom read as WKT is: "+theGeom);
    	return theGeom;
  
    }
    
    /**
     * Narrow to geometry.
     *
     * @param element the element
     * @return the geometry
     */
    private static Geometry narrowToGeometry(JSObject element) {
        return (element == null) ? null : new GeometryExt(element);
    }
    

    /**
     * Narrow to point.
     *
     * @param element the element
     * @return the point ext
     */
    private static PointExt narrowToPoint(JSObject element) {
        return (element == null) ? null : new PointExt(element);
    }
    

    /**
     * Geo JSO nto WKT.
     *
     * @param geoJsonString the geo json string
     * @return the string
     */
    public static native String geoJSONtoWKT(String geoJsonString) /*-{
    	console.log('geoJsonString: '+geoJsonString);
    	var geojson_format = new $wnd.OpenLayers.Format.GeoJSON();
    	var theFeature = geojson_format.read(geoJsonString);
    	//var geomJSON = geojson_format.parseGeometry(theFeature);
    	var wkt_options = {};
    	var wkt =  new $wnd.OpenLayers.Format.WKT(wkt_options);
    	var wktString = wkt.write(theFeature);
    	return wktString;
	}-*/;
    

    /**
     * Gets the centroid for geo JSON.
     *
     * @param geoJsonString the geo json string
     * @return the centroid for geo JSON
     */
    public static native JSObject getCentroidForGeoJSON(String geoJsonString) /*-{
    	console.log('geoJsonString: '+geoJsonString);
    	var geojson_format = new $wnd.OpenLayers.Format.GeoJSON();
    	var theFeature = geojson_format.read(geoJsonString);
    	//var geomJSON = geojson_format.parseGeometry(theFeature);
    	var wkt_options = {};
    	var wkt =  new $wnd.OpenLayers.Format.WKT(wkt_options);
    	var wktString = wkt.write(theFeature);
    	var geom = $wnd.OpenLayers.Geometry.fromWKT(wktString);
    	return geom.getCentroid();
	}-*/;
    
    
    
	/**
	 * Gets the JS object.
	 *
	 * @param geoJsonString the geo json string
	 * @return the JS object
	 */
	private JSObject getJSObject(String geoJsonString) {
		return FormatImpl.read(getJSObject(), geoJsonString);
	}
	
	/**
	 * To geo json string.
	 *
	 * @param geometryType the geometry type
	 * @param geometryCoordinate the geometry coordinate
	 * @return the string
	 */
	public static String toGeoJsonString(String geometryType, String geometryCoordinate) {
		
//		return $wnd.OpenLayers.Geometry.fromWKT(wkt);
		
//		JSONObject json = new JSONObject();
//		json.put("type", new com.google.gwt.json.client.JSONString("FeatureCollection"));
//		JSONArray features = new JSONArray();
//		json.put("features", features);
//		JSONObject feature = new JSONObject();
//		features.set(0, feature);
//		feature.put("type", new com.google.gwt.json.client.JSONString("Feature"));
//		JSONObject geometry = new JSONObject();
//		feature.put("geometry",  geometry);
//		geometry.put("type", new com.google.gwt.json.client.JSONString(geometryType));
//		geometry.put("coordinates", new com.google.gwt.json.client.JSONString(geometryCoordinate));
//		return json.toString();
		
		String geoJsonString = "{\"type\": \"FeatureCollection\","
				+ "\"features\": ["
				+ "{"
				+ "\"type\": \"Feature\","
				+ "\"geometry\": {"
				+ "\"type\": \""+geometryType+"\","
				+ "\"coordinates\": "+geometryCoordinate+""
				+ "},"
				+ "\"geometry_name\": \"the_geom\","
				+ "\"properties\": {}"
				+ "}]}";
   
		return geoJsonString;
	}

}
