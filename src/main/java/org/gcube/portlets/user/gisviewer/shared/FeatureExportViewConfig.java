package org.gcube.portlets.user.gisviewer.shared;

import java.io.Serializable;

public class FeatureExportViewConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3434624690746503690L;
	
	
	private String featureName;
	private boolean export;
	private String exportAs;
	public static String WILDCARD = "*";

	public FeatureExportViewConfig() {
	}

	public FeatureExportViewConfig(String featureName, boolean export, String exportAs) {
		super();
		this.featureName = featureName;
		this.export = export;
		this.exportAs = exportAs;
	}

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public boolean isExport() {
		return export;
	}

	public void setExport(boolean export) {
		this.export = export;
	}

	public String getExportAs() {
		return exportAs;
	}

	public void setExportAs(String exportAs) {
		this.exportAs = exportAs;
	}

	@Override
	public String toString() {
		return "FeatureExportViewConfig [featureName=" + featureName + ", export=" + export + ", exportAs=" + exportAs
				+ "]";
	}
	
	
}
