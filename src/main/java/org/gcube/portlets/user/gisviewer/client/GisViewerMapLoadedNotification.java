/**
 * 
 */
package org.gcube.portlets.user.gisviewer.client;


// TODO: Auto-generated Javadoc
/**
 * The Class GisViewerMapLoadedNotification.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Apr 21, 2020
 */
public class GisViewerMapLoadedNotification {

	
	/**
	 * The listener interface for receiving gisViewerMapLoadedt events.
	 * The class that is interested in processing a gisViewerMapLoadedt
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addGisViewerMapLoadedtListener<code> method. When
	 * the gisViewerMapLoadedt event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see GisViewerMapLoadedtEvent
	 */
	public interface GisViewerMapLoadedtListener {
		
		
		/**
		 * On map loaded. Fired when the OLM has been loaded
		 */
		void onMapLoaded();


		/**
		 * On failed.
		 *
		 * @param throwable the throwable
		 */
		void onFailed(Throwable throwable);
		
	}

	
	/**
	 * The listener interface for receiving hasGisViewerMapLoadedNotification events.
	 * The class that is interested in processing a hasGisViewerMapLoadedNotification
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addHasGisViewerMapLoadedNotificationListener<code> method. When
	 * the hasGisViewerMapLoadedNotification event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see HasGisViewerMapLoadedNotificationEvent
	 */
	public interface HasGisViewerMapLoadedListener {

		
		/**
		 * Adds the map loaded listener.
		 *
		 * @param handler the handler
		 */
		public void addMapLoadedListener(GisViewerMapLoadedtListener handler);

		
		/**
		 * Removes the map loaded listener.
		 *
		 * @param handler the handler
		 */
		public void removeMapLoadedListener(GisViewerMapLoadedtListener handler);

	}
	
}