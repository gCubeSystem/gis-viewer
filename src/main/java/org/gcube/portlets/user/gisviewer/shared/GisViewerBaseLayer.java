package org.gcube.portlets.user.gisviewer.shared;

import java.io.Serializable;
import java.util.Map;

import org.gcube.portlets.user.gisviewer.client.commons.beans.GisViewerBaseLayerInterface;

// TODO: Auto-generated Javadoc
/**
 * The Class GisViewerBaseLayer.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) Apr 21, 2020
 */
public class GisViewerBaseLayer implements GisViewerBaseLayerInterface, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3202101184874861425L;
	private Map<String, String> mapProperties;
	private String title;
	private String name;
	private String wmsServiceBaseURL;
	private boolean display;

	public GisViewerBaseLayer() {

	}

	public GisViewerBaseLayer(Map<String, String> mapProperties) {
		this.mapProperties = mapProperties;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWmsServiceBaseURL() {
		return wmsServiceBaseURL;
	}

	public void setWmsServiceBaseURL(String wmsServiceBaseURL) {
		this.wmsServiceBaseURL = wmsServiceBaseURL;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	public void setMapProperties(Map<String, String> mapProperties) {
		this.mapProperties = mapProperties;
	}

	public Map<String, String> getMapProperties() {
		return mapProperties;
	}

	@Override
	public String toString() {
		return "GisViewerBaseLayer [mapProperties=" + mapProperties + ", title=" + title + ", name=" + name
				+ ", wmsServiceBaseURL=" + wmsServiceBaseURL + ", display=" + display + "]";
	}

}
