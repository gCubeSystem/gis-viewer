package org.gcube.portlets.user.gisviewer.client.event;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface TableRowSelectedEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 12, 2020
 */
public interface TableRowSelectedEventHandler extends EventHandler {
	
	/**
	 * On table row selected.
	 *
	 * @param tableRowSelectedEvent the table row selected event
	 */
	void onTableRowSelected(TableRowSelectedEvent tableRowSelectedEvent);
}
