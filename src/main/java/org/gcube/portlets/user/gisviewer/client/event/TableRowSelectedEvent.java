package org.gcube.portlets.user.gisviewer.client.event;

import com.extjs.gxt.ui.client.data.ModelData;
import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class TableRowSelectedEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 12, 2020
 */
public class TableRowSelectedEvent extends GwtEvent<TableRowSelectedEventHandler> {
	public static Type<TableRowSelectedEventHandler> TYPE = new Type<TableRowSelectedEventHandler>();
	private ModelData target;

	/**
	 * Instantiates a new table row selected event.
	 *
	 * @param target the target
	 */
	public TableRowSelectedEvent(ModelData target) {
		this.target = target;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<TableRowSelectedEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(TableRowSelectedEventHandler handler) {
		handler.onTableRowSelected(this);

	}

	/**
	 * Gets the target.
	 *
	 * @return the target
	 */
	public ModelData getTarget() {
		return target;
	}
}