package org.gcube.portlets.user.gisviewer.client.openlayers;

import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * The Class PointExt.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 13, 2020
 */
public class PointExt extends Point{

	/**
	 * Instantiates a new point ext.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public PointExt(double x, double y) {
		super(x, y);
	}
	
	/**
	 * Instantiates a new point ext.
	 *
	 * @param point the point
	 */
	public PointExt(JSObject point){
	   super(point);
	}

}
