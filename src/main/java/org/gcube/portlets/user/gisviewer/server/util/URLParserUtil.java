package org.gcube.portlets.user.gisviewer.server.util;


/**
 * The Class URLParserUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 8, 2020
 */
public class URLParserUtil {
	
	
	/**
	 * Extract value of parameter from URL.
	 *
	 * @param paramName the param name
	 * @param url       the url
	 * @return the string
	 */
	public static String extractValueOfParameterFromURL(String paramName, String url) {
		int index = url.toLowerCase().indexOf(paramName.toLowerCase() + "="); // ADDING CHAR "=" IN TAIL TO BE SURE THAT IT
																				// IS A PARAMETER
		String value = "";
		if (index > -1) {

			int start = index + paramName.length() + 1; // add +1 for char '='
			String sub = url.substring(start, url.length());
			int indexOfSeparator = sub.indexOf("&");
			int end = indexOfSeparator != -1 ? indexOfSeparator : sub.length();
			value = sub.substring(0, end);
		} else
			return null;

		return value;
	}

}
