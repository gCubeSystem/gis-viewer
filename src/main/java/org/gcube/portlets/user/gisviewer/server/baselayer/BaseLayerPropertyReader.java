package org.gcube.portlets.user.gisviewer.server.baselayer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.gcube.portlets.user.gisviewer.server.exception.PropertyFileNotFoundException;
import org.gcube.portlets.user.gisviewer.server.util.CSVReader;
import org.gcube.portlets.user.gisviewer.server.util.FileUtil;
import org.gcube.portlets.user.gisviewer.shared.CSVFile;
import org.gcube.portlets.user.gisviewer.shared.CSVRow;
import org.gcube.portlets.user.gisviewer.shared.GisViewerBaseLayer;


// TODO: Auto-generated Javadoc
/**
 * The Class BaseLayerPropertyReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Apr 21, 2020
 */
public class BaseLayerPropertyReader {

	protected static final String BASE_LAYER_FILE = "baselayers.csv";

	public static Logger logger = Logger
			.getLogger(BaseLayerPropertyReader.class);
	
	/**
	 * Read base layers.
	 *
	 * @return the list<? extends gis viewer base layer>
	 * @throws PropertyFileNotFoundException the property file not found exception
	 */
	public static List<? extends GisViewerBaseLayer> readBaseLayers() throws PropertyFileNotFoundException {

		File baseLayersFile = null;
		
		try {
			
			InputStream in = (InputStream) BaseLayerPropertyReader.class.getResourceAsStream(BASE_LAYER_FILE);
			baseLayersFile = FileUtil.inputStreamToTempFile(in,BASE_LAYER_FILE);
			CSVReader reader = new CSVReader(baseLayersFile);
			CSVFile csvFile = reader.getCsvFile();
			List<GisViewerBaseLayer> listBaseLayers = new ArrayList<GisViewerBaseLayer>(csvFile.getValueRows().size());
			
			for (CSVRow row : csvFile.getValueRows()) {
				GisViewerBaseLayer baseLayer = new GisViewerBaseLayer();
				List<String> headerKeys = csvFile.getHeaderRow().getListValues();
				Map<String,String> mapProperties = new HashMap<String, String>(row.getListValues().size());
				int i = 0;
				//to map properties
				List<String> rowValues = row.getListValues();
				for (String value : rowValues) {
					mapProperties.put(headerKeys.get(i), value);
					i++;
				}
				baseLayer.setMapProperties(mapProperties);
				//adding data to interface
				try {
					baseLayer.setTitle(rowValues.get(0));
					baseLayer.setName(rowValues.get(1));
					baseLayer.setWmsServiceBaseURL(rowValues.get(2));
					baseLayer.setDisplay(Boolean.parseBoolean(rowValues.get(3)));
				}catch (Exception e) {
					logger.error("Has the property file " + BASE_LAYER_FILE + " been changed?", e);
				}
				
				listBaseLayers.add(baseLayer);
			}
			
			return listBaseLayers;

		} catch (IOException e) {
			logger.error("An error occurred on reading the property file " + BASE_LAYER_FILE, e);
			throw new PropertyFileNotFoundException(
					"Error on reading the base layers. Is the file '"+BASE_LAYER_FILE+ "' in the application path?");
		}finally {
			
			if(baseLayersFile!=null) {
				try {
					baseLayersFile.delete();
				} catch (Exception e) {
					//silent
				}
			}
		}
	}
	
}
