package org.gcube.portlets.user.gisviewer.client.openlayers;

public class Util {
	
	public static native String getFormattedLonLat(double coordinate, String axis, String dmsOption)/*-{
		return $wnd.OpenLayers.Util.getFormattedLonLat(coordinate, axis, dmsOption);
	}-*/;
	
	
	public static native String bboxToBounds(String array)/*-{
		return $wnd.OpenLayers.Bounds.fromArray(array);
	}-*/;
	
	public static native String jsonToGeometry(String geoJson)/*-{
	return $wnd.OpenLayers.Format.JSON(array);
}-*/;
}
