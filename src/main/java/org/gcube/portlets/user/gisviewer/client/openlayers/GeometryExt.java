package org.gcube.portlets.user.gisviewer.client.openlayers;

import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.util.JSObject;

// TODO: Auto-generated Javadoc
/**
 * The Class GeometryExt.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) May 12, 2020
 */
public class GeometryExt extends Geometry {

	/**
	 * Instantiates a new geometry ext.
	 *
	 * @param element the element
	 */
	public GeometryExt(JSObject element) {
		super(element);
	}

}
