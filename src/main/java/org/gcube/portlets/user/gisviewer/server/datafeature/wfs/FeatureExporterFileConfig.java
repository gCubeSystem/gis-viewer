package org.gcube.portlets.user.gisviewer.server.datafeature.wfs;

import java.util.HashMap;
import java.util.Map;

import org.gcube.portlets.user.gisviewer.server.datafeature.wfs.WFSExporter.WFS_TO;
import org.gcube.portlets.user.gisviewer.shared.CSVFile;
import org.gcube.portlets.user.gisviewer.shared.FeatureExportViewConfig;


/**
 * The Class FeatureExporterFileConfig.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 13, 2020
 */
public class FeatureExporterFileConfig {

	private CSVFile file;
	private Map<String, FeatureExportViewConfig> map = new HashMap<String, FeatureExportViewConfig>();
	private WFS_TO wfsTo;

	/**
	 * Instantiates a new feature exporter file config.
	 */
	public FeatureExporterFileConfig() {
	}

	/**
	 * Instantiates a new feature exporter file config.
	 *
	 * @param file the file
	 * @param map the map
	 * @param wfsTo the wfs to
	 */
	public FeatureExporterFileConfig(CSVFile file, Map<String, FeatureExportViewConfig> map, WFS_TO wfsTo) {
		super();
		this.file = file;
		this.map = map;
		this.wfsTo = wfsTo;
	}

	/**
	 * Gets the wfs to.
	 *
	 * @return the wfs to
	 */
	public WFS_TO getWfsTo() {
		return wfsTo;
	}

	/**
	 * Sets the wfs to.
	 *
	 * @param wfsTo the new wfs to
	 */
	public void setWfsTo(WFS_TO wfsTo) {
		this.wfsTo = wfsTo;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public CSVFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 *
	 * @param file the new file
	 */
	public void setFile(CSVFile file) {
		this.file = file;
	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public Map<String, FeatureExportViewConfig> getMap() {
		return map;
	}

	/**
	 * Sets the map.
	 *
	 * @param map the map
	 */
	public void setMap(Map<String, FeatureExportViewConfig> map) {
		this.map = map;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "FeatureExporterFileConfig [file=" + file + ", map=" + map + ", wfsTo=" + wfsTo + "]";
	}

	

}
