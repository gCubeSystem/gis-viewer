# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.5.0] - 2020-05-07

### Added

**Features**

[#19207] init parameter to set max zoom level

[#19220] CQL filtering (if activated for WMS) should be applied also for GetFeature request

[#19249] add a geometry into map when selecting a row in the data table


## [4.4.0] - 2020-04-21

### Added

**Features**

[#19111] init settings configurable via property file and GET parameters

[#19109] management and access to restricted data

### Changed

**Fixes**

[#19187] WFS export with the BBOX field clashing with csv separator

